/**
 * Autogenerated by Thrift Compiler (0.9.1)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Thrift;
using Thrift.Collections;
using System.Runtime.Serialization;
using Thrift.Protocol;
using Thrift.Transport;

namespace idl.alarm.admin.tmstruct
{

  #if !SILVERLIGHT
  [Serializable]
  #endif
  public partial class CustomerInfo : TBase
  {
    private string _id;
    private string _password;
    private string _companyPhone;
    private idl.alarm.admin.tmenum.BankCodeType _bankCodeType;
    private idl.alarm.admin.tmenum.CustemerType _customerType;

    public string Id
    {
      get
      {
        return _id;
      }
      set
      {
        __isset.id = true;
        this._id = value;
      }
    }

    public string Password
    {
      get
      {
        return _password;
      }
      set
      {
        __isset.password = true;
        this._password = value;
      }
    }

    public string CompanyPhone
    {
      get
      {
        return _companyPhone;
      }
      set
      {
        __isset.companyPhone = true;
        this._companyPhone = value;
      }
    }

    /// <summary>
    /// 
    /// <seealso cref="idl.alarm.admin.tmenum.BankCodeType"/>
    /// </summary>
    public idl.alarm.admin.tmenum.BankCodeType BankCodeType
    {
      get
      {
        return _bankCodeType;
      }
      set
      {
        __isset.bankCodeType = true;
        this._bankCodeType = value;
      }
    }

    /// <summary>
    /// 
    /// <seealso cref="idl.alarm.admin.tmenum.CustemerType"/>
    /// </summary>
    public idl.alarm.admin.tmenum.CustemerType CustomerType
    {
      get
      {
        return _customerType;
      }
      set
      {
        __isset.customerType = true;
        this._customerType = value;
      }
    }


    public Isset __isset;
    #if !SILVERLIGHT
    [Serializable]
    #endif
    public struct Isset {
      public bool id;
      public bool password;
      public bool companyPhone;
      public bool bankCodeType;
      public bool customerType;
    }

    public CustomerInfo() {
    }

    public void Read (TProtocol iprot)
    {
      TField field;
      iprot.ReadStructBegin();
      while (true)
      {
        field = iprot.ReadFieldBegin();
        if (field.Type == TType.Stop) { 
          break;
        }
        switch (field.ID)
        {
          case 1:
            if (field.Type == TType.String) {
              Id = iprot.ReadString();
            } else { 
              TProtocolUtil.Skip(iprot, field.Type);
            }
            break;
          case 2:
            if (field.Type == TType.String) {
              Password = iprot.ReadString();
            } else { 
              TProtocolUtil.Skip(iprot, field.Type);
            }
            break;
          case 3:
            if (field.Type == TType.String) {
              CompanyPhone = iprot.ReadString();
            } else { 
              TProtocolUtil.Skip(iprot, field.Type);
            }
            break;
          case 4:
            if (field.Type == TType.I32) {
              BankCodeType = (idl.alarm.admin.tmenum.BankCodeType)iprot.ReadI32();
            } else { 
              TProtocolUtil.Skip(iprot, field.Type);
            }
            break;
          case 5:
            if (field.Type == TType.I32) {
              CustomerType = (idl.alarm.admin.tmenum.CustemerType)iprot.ReadI32();
            } else { 
              TProtocolUtil.Skip(iprot, field.Type);
            }
            break;
          default: 
            TProtocolUtil.Skip(iprot, field.Type);
            break;
        }
        iprot.ReadFieldEnd();
      }
      iprot.ReadStructEnd();
    }

    public void Write(TProtocol oprot) {
      TStruct struc = new TStruct("CustomerInfo");
      oprot.WriteStructBegin(struc);
      TField field = new TField();
      if (Id != null && __isset.id) {
        field.Name = "id";
        field.Type = TType.String;
        field.ID = 1;
        oprot.WriteFieldBegin(field);
        oprot.WriteString(Id);
        oprot.WriteFieldEnd();
      }
      if (Password != null && __isset.password) {
        field.Name = "password";
        field.Type = TType.String;
        field.ID = 2;
        oprot.WriteFieldBegin(field);
        oprot.WriteString(Password);
        oprot.WriteFieldEnd();
      }
      if (CompanyPhone != null && __isset.companyPhone) {
        field.Name = "companyPhone";
        field.Type = TType.String;
        field.ID = 3;
        oprot.WriteFieldBegin(field);
        oprot.WriteString(CompanyPhone);
        oprot.WriteFieldEnd();
      }
      if (__isset.bankCodeType) {
        field.Name = "bankCodeType";
        field.Type = TType.I32;
        field.ID = 4;
        oprot.WriteFieldBegin(field);
        oprot.WriteI32((int)BankCodeType);
        oprot.WriteFieldEnd();
      }
      if (__isset.customerType) {
        field.Name = "customerType";
        field.Type = TType.I32;
        field.ID = 5;
        oprot.WriteFieldBegin(field);
        oprot.WriteI32((int)CustomerType);
        oprot.WriteFieldEnd();
      }
      oprot.WriteFieldStop();
      oprot.WriteStructEnd();
    }

    public override string ToString() {
      StringBuilder sb = new StringBuilder("CustomerInfo(");
      sb.Append("Id: ");
      sb.Append(Id);
      sb.Append(",Password: ");
      sb.Append(Password);
      sb.Append(",CompanyPhone: ");
      sb.Append(CompanyPhone);
      sb.Append(",BankCodeType: ");
      sb.Append(BankCodeType);
      sb.Append(",CustomerType: ");
      sb.Append(CustomerType);
      sb.Append(")");
      return sb.ToString();
    }

  }

}
