include "tmenum.thrift"

namespace csharp idl.alarm.client.tmstruct
namespace java idl.alarm.client.tmstruct


struct ClientInfo{
	1: string		id;
	2: string		password;
	3: string		phoneNum;
	4: string		name;
	5: string		deviceToken;
	6: string		email;
}

struct ClientMessageInfo{
	1: tmenum.MessageType	messageType;
	2: i32				companyCode
	3: string			msg;
	4: string			uuid;
}

struct ClientMessageDBInfo{
	1: string		id;
	2: string		password;
	3: string		phoneNum;
	4: string		name;
	5: string		deviceToken;
	6: string		email;
}