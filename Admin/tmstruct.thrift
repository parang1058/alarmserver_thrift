include "tmenum.thrift"

namespace csharp idl.alarm.admin.tmstruct
namespace java idl.alarm.admin.tmstruct

struct CustomerInfo{
	1: string				id;
	2: string				password;
	3: string				companyPhone;
	4: tmenum.BankCodeType		bankCodeType;
	5: tmenum.CustemerType		customerType;
}