namespace csharp idl.alarm.admin.tmexception
namespace java idl.alarm.admin.tmexception

exception AdminException{
	1: string	message;
	2: string	stacktrace;
	3: string	source;
}