namespace csharp idl.alarm.customer.tmexception
namespace java idl.alarm.customer.tmexception

exception CustomerException{
	1: string	message;
	2: string	stacktrace;
	3: string	source;
}