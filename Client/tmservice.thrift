include "tmenum.thrift"
include "tmstruct.thrift"
include "tmexception.thrift"

namespace csharp idl.alarm.client.tmservice
namespace java idl.alarm.client.tmservice

service AlrimClientHandler{
	tmstruct.ClientMessageInfo getMessage(1: string phoneNum, 2: string messageUUID) throws(1: tmexception.ClientException e),
	list<tmstruct.ClientMessageInfo> getMessages(1: string phoneNum, 2: list<string> messageUUID) throws (1: tmexception.ClientException e)
}