include "tmstruct.thrift"
include "tmenum.thrift"
include "tmexception.thrift"

namespace csharp idl.alarm.customer.tmservice
namespace java idl.alarm.customer.tmservice

service  AlrimCustomerServiceHandler{
	bool requestPush(1: string id, 2: string password, 3: string phoneNumber, 4: tmenum.MessageType messageType, 5: string msg) throws (1: tmexception.CustomerException e),
	list<string> requestPushs(1: string id, 2: string password, 3: list<string> phoneNumber, 4: tmenum.MessageType messageType, 5: string msg) throws (1: tmexception.CustomerException e)
}