namespace csharp idl.alarm.admin.tmenum
namespace java idl.alarm.admin.tmenum


/*은행종류....추가해야됨.*/
enum BankCodeType{
	KOREA = 1,
	KDB = 2,
	IBK = 3,
	KB = 4,
	KEB = 5,
	SUHYUP = 7,
	KOREAEXIM = 8,
	NONGHYUP = 11,
	WOORI = 20,
	SC = 23,
	CITY = 27,
	DAEGU = 31,
	BUSAN = 32,
	GWANGJU = 34,
	JEJU = 35,
	JEONBUK = 37,
	GYEONGNAM = 39
}

/*금융기관 종류... 추가해야됨.*/
enum CustemerType{
	BANKING = 1,
	SECURITY = 2,
	INSURANCE = 4	
}