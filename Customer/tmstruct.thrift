include "tmenum.thrift"

namespace csharp idl.alarm.customer.tmstruct
namespace java idl.alarm.customer.tmstruct


/*전체적인 메시지 정보*/
struct MessageInfo{
	1: tmenum.MessageType	messageType;
	2: i32			companyCode;
	3: string		msg;
	4: string		UUID
}

/* 싱글푸시...*/
struct RequestInfo{
	1: string		id;
	2: string		password;
	3: string		phoneNumber;
	4: MessageInfo	messgaInfo;
}

/* 멀티푸시...*/
struct RequestGroupInfo{
	1: string		id;
	2: string		password;
	3: list<string>	phoneNumber;
	4: MessageInfo	messgaInfo;
}

/*메시지 디비정보*/
struct MessageDBInfo{
	1: string		msg;
	2: i32			companyCode;
	3: list<string>	phoneNumber;
	4: string		MessageUUID;
	5: tmenum.MessageType	messageType;
}


