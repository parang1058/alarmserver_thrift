namespace csharp idl.alarm.client.tmexception
namespace java idl.alarm.client.tmexception

exception ClientException{
	1: string	message;
	2: string	stacktrace;
	3: string	source;
}