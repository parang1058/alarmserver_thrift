/**
 * Autogenerated by Thrift Compiler (0.9.1)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Thrift;
using Thrift.Collections;
using System.Runtime.Serialization;
using Thrift.Protocol;
using Thrift.Transport;

namespace idl.alarm.customer.tmstruct
{

  #if !SILVERLIGHT
  [Serializable]
  #endif
  public partial class RequestInfo : TBase
  {
    private string _id;
    private string _password;
    private string _phoneNumber;
    private MessageInfo _messgaInfo;

    public string Id
    {
      get
      {
        return _id;
      }
      set
      {
        __isset.id = true;
        this._id = value;
      }
    }

    public string Password
    {
      get
      {
        return _password;
      }
      set
      {
        __isset.password = true;
        this._password = value;
      }
    }

    public string PhoneNumber
    {
      get
      {
        return _phoneNumber;
      }
      set
      {
        __isset.phoneNumber = true;
        this._phoneNumber = value;
      }
    }

    public MessageInfo MessgaInfo
    {
      get
      {
        return _messgaInfo;
      }
      set
      {
        __isset.messgaInfo = true;
        this._messgaInfo = value;
      }
    }


    public Isset __isset;
    #if !SILVERLIGHT
    [Serializable]
    #endif
    public struct Isset {
      public bool id;
      public bool password;
      public bool phoneNumber;
      public bool messgaInfo;
    }

    public RequestInfo() {
    }

    public void Read (TProtocol iprot)
    {
      TField field;
      iprot.ReadStructBegin();
      while (true)
      {
        field = iprot.ReadFieldBegin();
        if (field.Type == TType.Stop) { 
          break;
        }
        switch (field.ID)
        {
          case 1:
            if (field.Type == TType.String) {
              Id = iprot.ReadString();
            } else { 
              TProtocolUtil.Skip(iprot, field.Type);
            }
            break;
          case 2:
            if (field.Type == TType.String) {
              Password = iprot.ReadString();
            } else { 
              TProtocolUtil.Skip(iprot, field.Type);
            }
            break;
          case 3:
            if (field.Type == TType.String) {
              PhoneNumber = iprot.ReadString();
            } else { 
              TProtocolUtil.Skip(iprot, field.Type);
            }
            break;
          case 4:
            if (field.Type == TType.Struct) {
              MessgaInfo = new MessageInfo();
              MessgaInfo.Read(iprot);
            } else { 
              TProtocolUtil.Skip(iprot, field.Type);
            }
            break;
          default: 
            TProtocolUtil.Skip(iprot, field.Type);
            break;
        }
        iprot.ReadFieldEnd();
      }
      iprot.ReadStructEnd();
    }

    public void Write(TProtocol oprot) {
      TStruct struc = new TStruct("RequestInfo");
      oprot.WriteStructBegin(struc);
      TField field = new TField();
      if (Id != null && __isset.id) {
        field.Name = "id";
        field.Type = TType.String;
        field.ID = 1;
        oprot.WriteFieldBegin(field);
        oprot.WriteString(Id);
        oprot.WriteFieldEnd();
      }
      if (Password != null && __isset.password) {
        field.Name = "password";
        field.Type = TType.String;
        field.ID = 2;
        oprot.WriteFieldBegin(field);
        oprot.WriteString(Password);
        oprot.WriteFieldEnd();
      }
      if (PhoneNumber != null && __isset.phoneNumber) {
        field.Name = "phoneNumber";
        field.Type = TType.String;
        field.ID = 3;
        oprot.WriteFieldBegin(field);
        oprot.WriteString(PhoneNumber);
        oprot.WriteFieldEnd();
      }
      if (MessgaInfo != null && __isset.messgaInfo) {
        field.Name = "messgaInfo";
        field.Type = TType.Struct;
        field.ID = 4;
        oprot.WriteFieldBegin(field);
        MessgaInfo.Write(oprot);
        oprot.WriteFieldEnd();
      }
      oprot.WriteFieldStop();
      oprot.WriteStructEnd();
    }

    public override string ToString() {
      StringBuilder sb = new StringBuilder("RequestInfo(");
      sb.Append("Id: ");
      sb.Append(Id);
      sb.Append(",Password: ");
      sb.Append(Password);
      sb.Append(",PhoneNumber: ");
      sb.Append(PhoneNumber);
      sb.Append(",MessgaInfo: ");
      sb.Append(MessgaInfo== null ? "<null>" : MessgaInfo.ToString());
      sb.Append(")");
      return sb.ToString();
    }

  }

}
